class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  def hello
    render html: "Olá universo!" + Time.current.to_s
  end


  def carlos
    @aaa = "Ola Gabriel"
    render "carlos"
  end
end
