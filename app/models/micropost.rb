class Micropost < ApplicationRecord
  validates :content, length: { maximum: 137 },
                      presence: true
  belongs_to :user
end
